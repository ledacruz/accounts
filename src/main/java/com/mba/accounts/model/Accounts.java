package com.mba.accounts.model;


import lombok.*;

@Data
@Builder
public class Accounts {

    public String eventType;
    public String timestamp;
}
