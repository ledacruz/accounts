package com.mba.accounts.controller;

import com.google.gson.Gson;
import com.mba.accounts.messaging.AccountsProducer;
import com.mba.accounts.model.Accounts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@RestController
    @RequestMapping(value = "/kafka")
    public class AccountsController {

        private final AccountsProducer accountsProducer;

        @Autowired
        public AccountsController(AccountsProducer accountsProducer) {
            this.accountsProducer = accountsProducer;
        }
        @PostMapping(value = "/publish/cho/accounts")
        public void sendMessageToKafkaTopic(@RequestParam("message") String message){
            this.accountsProducer.sendMessage(convertAccountsToString(message), "choreography-topic");
        }

        private String convertAccountsToString(String message){
            Gson gson = new Gson();
            Accounts accounts = Accounts.builder().eventType(message).timestamp(getDateTime()).build();;
            String finalMessage = gson.toJson(accounts);
            return finalMessage;
        }

    private String getDateTime() {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy-HH:mm:ss.SSS");
        Date date = new Date();
        return dateFormat.format(date);
    }
    }
