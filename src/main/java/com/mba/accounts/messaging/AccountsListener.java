package com.mba.accounts.messaging;

import com.google.gson.Gson;
import com.mba.accounts.model.Accounts;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class AccountsListener {

    @Autowired
    AccountsProducer accountsProducer;

    @Value("${topic-choreography}")
    private String choreographyTopic;

    @Value("${topic-out}")
    private String orchestratorTopic;

    private final Logger logger = LoggerFactory.getLogger(AccountsListener.class);

    private String producerMessage;

    @KafkaListener(topics = "${topic-choreography}", groupId = "group-accounts")
    public void consumeChoreography(String message){

        Accounts accounts = convertStringtoAccounts(message);

        switch (accounts.getEventType()) {
            case "transfer.received":
                producerMessage = "accounts.sucess";
                //producerMessage = "accounts.failure";
                callProducer(producerMessage, choreographyTopic);
                break;
            case "receipts.failure":
                producerMessage = "accounts.undo.sucess";
                //producerMessage = "accounts.undo.failure";
                callProducer(producerMessage, choreographyTopic);
                break;
            case "fraud.failure":
                producerMessage = "accounts.undo.sucess";
                //producerMessage = "accounts.undo.failure";
                callProducer(producerMessage, choreographyTopic);
                break;
            case "transferSender.failure":
                producerMessage = "accounts.undo.sucess";
                //producerMessage = "accounts.undo.failure";
                callProducer(producerMessage, choreographyTopic);
                break;
            default:
                logger.info(String.format("$$ -> Consumed not a know Message -> %s",message));
        }

    }

    @KafkaListener(topics = "${topic-in}", groupId = "group-accounts")
    public void consumeOrchestration(String message){
        logger.info(String.format("$$ -> Consumed Message -> %s",message));

        Accounts accounts = convertStringtoAccounts(message);

        switch (accounts.getEventType()) {
            case "accounts.requested":
                producerMessage = "accounts.sucess";
                //producerMessage = "accounts.failure";
                callProducer(producerMessage, orchestratorTopic);
                break;
            case "undo.requested":
                producerMessage = "accounts.undo.sucess";
                //producerMessage = "accounts.undo.failure";
                callProducer(producerMessage, orchestratorTopic);
                break;
        }
    }

    private void callProducer(String message, String topic){

        accountsProducer.sendMessage(producerMessage, topic);
    }

    private Accounts convertStringtoAccounts(String message){
        Gson gson = new Gson();
        Accounts accounts = gson.fromJson(message, Accounts.class);

        return accounts;

    }


}
