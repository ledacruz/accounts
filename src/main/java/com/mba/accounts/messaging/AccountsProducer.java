package com.mba.accounts.messaging;

import com.google.gson.Gson;
import com.mba.accounts.model.Accounts;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j
@Service
public class AccountsProducer {
    private static final Logger logger = LoggerFactory.getLogger(AccountsProducer.class);

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    public void sendMessage(String message, String topic){

        String record = convertAccountsToString(message);

        logger.info(String.format("$$ -> Producing message --> %s", record));
        this.kafkaTemplate.send(topic, record);
    }

    private String getDateTime() {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy-HH:mm:ss.SSS");
        Date date = new Date();
        return dateFormat.format(date);
    }

    private String convertAccountsToString(String message){

        Gson gson = new Gson();
        Accounts accounts = Accounts.builder().eventType(message).timestamp(getDateTime()).build();;
        String finalMessage = gson.toJson(accounts);

        return finalMessage;
    }
}